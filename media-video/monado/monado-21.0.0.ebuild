# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="Monado XR Runtime"
HOMEPAGE="https://gitlab.freedesktop.org/monado/monado/"
SRC_URI="https://gitlab.freedesktop.org/monado/monado/-/archive/v${PV}/monado-v${PV}.tar.gz"
inherit git-r3

LICENSE="Boost-1.0"
SLOT="0"
KEYWORDS="~amd64"
IUSE="-psvr -realsense -vive -openhmd -v4l2 -daydream -arduino -illixr -ultraleap -hdk -psmove -hydra -northstar"

DEPEND="
	media-libs/vulkan-loader
	dev-util/vulkan-headers
	virtual/opengl
	dev-cpp/eigen
	dev-util/glslang
	dev-libs/libusb
	virtual/udev
	vive? ( dev-libs/libsurvive )
	v4l2? ( media-libs/libv4l)
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	unpack ${A}
	mv monado-v${PV} ${P}
}

src_configure() {
	local mycmakeargs=(
		-DXRT_BUILD_DRIVER_VIVE=OFF
		-DXRT_BUILD_DRIVER_WMR=OFF
		-DXRT_BUILD_DRIVER_PSVR=$(usex psvr)
		-DXRT_BUILD_DRIVER_SURVIVE=$(usex vive)
		-DXRT_BUILD_DRIVER_RS=$(usex realsense)
		-DXRT_BUILD_DRIVER_OHMD=$(usex openhmd)
		-DXRT_BUILD_DRIVER_HANDTRACKING=$(usex v4l2)
		-DXRT_BUILD_DRIVER_DAYDREAM=$(usex daydream)
		-DXRT_BUILD_DRIVER_ARDUINO=$(usex arduino)
		-DXRT_BUILD_DRIVER_ILLIXR=$(usex illixr)
		-DXRT_BUILD_DRIVER_ULV2=$(usex ultraleap)
		-DXRT_BUILD_DRIVER_HDK=$(usex hdk)
		-DXRT_BUILD_DRIVER_PSMV=$(usex psmove)
		-DXRT_BUILD_DRIVER_HYDRA=$(usex hydra)
		-DXRT_BUILD_DRIVER_NS=$(usex northstar)
	)
	cmake_src_configure
}
