# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="OpenXR SDK"
HOMEPAGE="https://github.com/KhronosGroup/OpenXR-SDK-Source"
EGIT_REPO_URI="https://github.com/KhronosGroup/OpenXR-SDK-Source"
inherit git-r3

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="x11-base/xorg-server media-libs/mesa media-libs/vulkan-loader"
RDEPEND="${DEPEND}"
BDEPEND=""
