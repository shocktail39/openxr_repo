# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="6DoF tracking for Vive and Index"
HOMEPAGE="https://github.com/cntools/libsurvive"
SRC_URI="https://github.com/cntools/libsurvive/archive/refs/tags/v0.4.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="
x11-base/xorg-server
sys-libs/zlib
dev-libs/libusb
media-libs/freeglut
virtual/lapacke
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
	local mycmakeargs=(-DCMAKE_INSTALL_PREFIX=/usr -DBUILD_STATIC=ON -GNinja)
	cmake_src_configure
}
